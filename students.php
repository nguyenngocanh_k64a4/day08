<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Danh sách Sinh viên</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        *:focus {
            outline: none;
        }

        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 80vh;
        }

        .wrapper {
            width: 40%;
        }

        .form-search {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .field {
            margin-bottom: 25px;
            display: flex;
            align-items: center;
        }

        .field__label {
            padding: 8px 6px;
            width: 120px;
        }

        .field__input {
            font-size: 14px;
            font-weight: 400;
            padding: 8px;
            border-radius: 0;
            border: 1px solid #3f6db9;
            width: 250px;
        }

        .field > div {
            width: 58%;
        }

        select {
            width: 250px;
            height: 43px;
            border: 1px solid #3f6db9;
        }

        .button {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .btn-submit {
            font-size: 16px;
            color: #eeeeee;
            background-color: #3f6db9;
            padding: 10px 28px;
            border-radius: 10px;
            margin-right: 10px;
            margin-left: 10px;
        }

        .btn-add {
            font-size: 14px;
            color: #eeeeee;
            background-color: #3f6db9;
            padding: 10px 24px;
            border-radius: 10px;
        }

        .btn-action {
            font-size: 14px;
            color: #eeeeee;
            background-color: #5c89d5;
            padding: 8px 20px;
        }

        .result {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-top: 30px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        .list-student {
            margin-top: 30px;
        }

    </style>
</head>
<body>
    <div class="main">
        <?php
            session_start();

            $faculties = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");

            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $_SESSION = $_POST;
            }

        ?>
        <div class="wrapper">
            <div class="form-search">
                <form method="POST">
                    <div class="field">
                        <label for="faculty" class="field__label">Phân khoa</label>
                        <select id="faculty" name="faculty">
                            <option value="0"></option>
                            <?php
                                foreach($faculties as $x => $x_value) {
                                    if ($_SESSION["faculty"] == $x_value) {
                                        echo '<option selected value="' . $x_value . '">' . $x_value . '</option>';
                                    }
                                    else echo '<option value="' . $x_value . '">' . $x_value . '</option>';
                                }
                            ?>
                        </select>
                    </div>

                    <div class="field">
                        <label for="search" class="field__label">Từ khóa</label>
                        <?php
                            if ($_SESSION["search"] != "") {
                                echo '<input type="text" name="search" id="search" class="field__input" value="' . $_SESSION["search"] .'"/>';
                            }
                            else echo '<input type="text" name="search" id="search" class="field__input"/>';
                        ?>
                    </div>

                    <div class="button">
                        <button type="button" id="btn-delete" class="btn-submit" onclick="clearData()">Xóa</button>
                        <button type="submit" id="btn-search" class="btn-submit">Tìm kiếm</button>
                    </div>
                </form>
            </div>

            <div class="result">
                <div>
                    <h4>Số sinh viên tìm thấy: xxx</h4>
                </div>

                <div>
                    <a href="register.php">
                        <input type="button" class="btn-add" value="Thêm" />
                    </a>
                </div>
            </div>

            <div class="list-student">
                <table>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <button class="btn-action">Xóa</button>
                            <button class="btn-action">Sửa</button>
                        </td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>Trần Thị B</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <button class="btn-action">Xóa</button>
                            <button class="btn-action">Sửa</button>
                        </td>
                    </tr>

                    <tr>
                        <td>3</td>
                        <td>Nguyễn Hoàng C</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <button class="btn-action">Xóa</button>
                            <button class="btn-action">Sửa</button>
                        </td>
                    </tr>

                    <tr>
                        <td>4</td>
                        <td>Đinh Quang D</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <button class="btn-action">Xóa</button>
                            <button class="btn-action">Sửa</button>
                        </td>
                    </tr>
                </table>
            </div>

        </div>

    </div>



</body>
<script>

    const faculty = document.getElementById("faculty");
    const search = document.getElementById("search");

    function clearData() {
        faculty.value = "0";
        search.value = "";
    };

</script>
</html>